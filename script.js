function openNav() {
document.getElementById("myNav").style.display = "block";
document.getElementById("footer").style.display = "none";
document.getElementById("main4").style.display = "none";
document.getElementById("main3").style.display = "none";
document.getElementById("main2").style.display = "none";
document.getElementById("main1").style.display = "none";
}

function closeNav() {
document.getElementById("myNav").style.display = "none";
document.getElementById("footer").style.display = "flex";
document.getElementById("main4").style.display = "flex";
document.getElementById("main3").style.display = "flex";
document.getElementById("main2").style.display = "flex";
document.getElementById("main1").style.display = "flex";
}

function myFunction() {
document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {
document.getElementById("myDropdown2").classList.toggle("show");
}
function myFunction3() {
document.getElementById("myDropdown3").classList.toggle("show");
}
function myFunction4() {
document.getElementById("myDropdown4").classList.toggle("show");
}
function myFunction5() {
document.getElementById("myDropdown5").classList.toggle("show");
}
// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
if (!event.target.matches(".dropbtn")) {
  var dropdowns = document.getElementsByClassName("dropdown-content");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
    var openDropdown = dropdowns[i];
    if (openDropdown.classList.contains("show")) {
      openDropdown.classList.remove("show");
    }
  }
}
};

//PriceCard
document.addEventListener("DOMContentLoaded", function () {
const numberDisplay = document.getElementById("numberDisplay");
const numberDisplay1 = document.getElementById("numberDisplay1");
const numberDisplay2 = document.getElementById("numberDisplay2");
const toggleCheckbox = document.getElementById("cbx-3");
let currentNumber = 29;
let interval;

toggleCheckbox.addEventListener("change", function () {
  if (toggleCheckbox.checked) {
    updateNumber();
    interval = setInterval(increaseNumber, 6);
  } else {
    clearInterval(interval);
    updateNumber();
    interval = setInterval(decreaseNumber, 6);
  }
});

function increaseNumber() {
  currentNumber++;
  if (currentNumber > 48 || currentNumber < 30) {
    clearInterval(interval);
  }
  updateNumber();
}

function decreaseNumber() {
  currentNumber--;
  if (currentNumber < 30 || currentNumber > 48) {
    clearInterval(interval);
  }
  updateNumber();
}

function updateNumber() {
  numberDisplay.textContent = currentNumber;
  numberDisplay1.textContent = currentNumber;
  numberDisplay2.textContent = currentNumber;
}
});


//Dynamictext
var dynamicTextElement = document.getElementById('dynamic-text');
  years = ["founder.", "developers.", "designers."];
currentdtextIndex = 0;
currentdtext = years[currentdtextIndex];
isDeleting = false;
charIndex = 0;

function updateDynamicText() {
if (isDeleting) {
  currentdtext = currentdtext.slice(0, -1);
  charIndex--;
} else {
  currentdtext = years[currentdtextIndex].substring(0, charIndex + 1);
  charIndex++;
}
dynamicTextElement.textContent = currentdtext;
if (!isDeleting && currentdtext === years[currentdtextIndex]) {
  isDeleting = true;
  setTimeout(updateDynamicText, 1200); // Wait before starting to delete
} else if (isDeleting && currentdtext === "") {
  isDeleting = false;
  charIndex = 0;
  currentdtextIndex = (currentdtextIndex + 1) % years.length;
  currentdtext = "";
  setTimeout(updateDynamicText, 500); // Wait before typing again
} else {
  const delay = isDeleting ? 50 : 150;
  setTimeout(updateDynamicText, delay);
}
}
updateDynamicText();

// Animation
AOS.init();




//Numcount
var counter1 = document.getElementById("counter1");
counter2 = document.getElementById("counter2");
counter3 = document.getElementById("counter3");
counter4 = document.getElementById("counter4");
counter5 = document.getElementById("counter5");
counter6 = document.getElementById("counter6");
counter7 = document.getElementById("counter7");
counter8 = document.getElementById("counter8");
counter9 = document.getElementById("counter9");

// Khi trang được cuộn
window.addEventListener("scroll", () => {
const scrollY = window.scrollY;
const windowHeight = window.innerHeight; // Chiều cao của cửa sổ trình duyệt

// Tính toán một ngưỡng để kích hoạt hiển thị số đếm
const threshold = windowHeight * 1; // Hiển thị số đếm khi cách màn hình 20% chiều cao cửa sổ

// Nếu số đếm chưa hiển thị và ngưỡng được vượt qua
if (scrollY >= threshold && counter1.textContent === "0" ) {
  animateCounter();
}
});

// Hàm tăng số đếm từ 0 lên 100
function animateCounter() {
let currentValue = 0;
let currentValue2 = 0;

const targetValue = 100;
const targetValue2 = 24;
const duration = 2500; // Thời gian hoàn thành (ms)
const increment = targetValue / (duration / 18); // Mỗi bước tăng tối đa
const increment2 = targetValue / (duration / 4); // Mỗi bước tăng tối đa

const interval = setInterval(() => {
  if (currentValue >= targetValue) {
    counter1.textContent = targetValue;
    clearInterval(interval);
  } else {
    currentValue += increment;
    counter1.textContent = Math.round(currentValue);
    counter3.textContent = Math.round(currentValue);
    counter4.textContent = Math.round(currentValue);
    counter6.textContent = Math.round(currentValue);
    counter7.textContent = Math.round(currentValue);
    counter9.textContent = Math.round(currentValue);
  }
}, 18); // Mỗi frame (khoảng 16ms)

const interval2 = setInterval(() => {
  if (currentValue2 >= targetValue2) {
    counter2.textContent = targetValue2;
    clearInterval(interval2);
  } else {
    currentValue2 += increment2;
    counter2.textContent = Math.round(currentValue2);
    counter5.textContent = Math.round(currentValue2);
    counter8.textContent = Math.round(currentValue2);
  }
}, 18); // Mỗi frame (khoảng 16ms)

}
