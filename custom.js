document.getElementById("userForm").addEventListener("submit", function(event) {


    event.preventDefault(); // Ngăn chặn việc submit form
  
    // Lấy dữ liệu từ input fields
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
  
    // In dữ liệu vào console
    var isValid = true;
              
    // Validate Name
    if (name === "") {
        document.getElementById("nameError").textContent = "Name is required";
        isValid = false;
    } else {
        document.getElementById("nameError").textContent = "";
    }
    
    // Validate Email
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!email.match(emailPattern)) {
        document.getElementById("emailError").textContent = "Invalid email address";
        isValid = false;
    } else {
        document.getElementById("emailError").textContent = "";
    }
    
    // Validate Password
    if (password.length < 8) {
        document.getElementById("passwordError").textContent = "Password must be at least 8 characters long";
        isValid = false;
    } else {
        document.getElementById("passwordError").textContent = "";
    }
    if (isValid) {
      console.log("Name: " + name);
      console.log("Email: " + email);
      console.log("Password: " + password);
  }
    // Có thể thực hiện xử lý dữ liệu hoặc gửi dữ liệu lên máy chủ ở đây
  });


var slideIndex = 0;
var currentSlideIndex = 0;
var slideArray = [];

// Hàm này sẽ giúp chúng ta tạo ra các đối tượng slide
// bao gồm: tiêu đề, mô tả, ảnh, đường dẫn khi nhấp vào button trên slide, 
// và id của mỗi slide
function Slide(image,comment,person) {
  this.image = image;
  this.comment = comment;
  this.person = person;
  // we need an id to target later using getElementById
  this.id = "slide" + slideIndex;
  // Add one to the index for the next slide number
  slideIndex++;
  // Add this Slide to our array
  slideArray.push(this);
}


// Tạo các đối tượng slide, bạn có thể tạo nhiều hơn

var Airbnb = new Slide(
  "/img/Air.png",
  "Landkit is hands down the most useful front end Bootrap theme I've ever used. I can't wait to use it agian for my next project",
  "DAVE GAMACHE",
);

var Insta = new Slide(
  "/img/Ins.png",
  "I've never used a theme as versatile and flexible as Landkit. It's my go to for building landing sites on almost any project",
  "RUSS D'SA",
);
// Từ mảng slide đã tạo, ta tiến hành đưa nó vào source HTML
function buildSlider() {
  // A variable to hold all our HTML
  var myHTML;

  // Go through the Array and add the code to our HTML
  for (var i = 0; i < slideArray.length; i++) {
    myHTML +=
    "<div id='" +
      slideArray[i].id +
      "' class='singleSlide'> <div class='slideOverlay'>" +
      " <img src='" + slideArray[i].image + "' style='width: 40%;'>" +
      "<h6 style='color: black;font-size: 16px;line-height: 1.8;'>\"" + slideArray[i].comment + "\"</h6>" +
      "<h10>\"" + slideArray[i].person + "\" </h10>" +
      "</div> </div>";
}

  // Đưa HTML chúng ta vừa tạo vào id #mySlider
  document.getElementById("mySlider").innerHTML = myHTML;

  // Đồng thời hiển thị slide đầu tiên
  document.getElementById("slide" + currentSlideIndex).style.left = 0;
}

// Gọi hàm thực thi
buildSlider();

let slideIndex1 = 1;
showSlides(1);

function showSlides(n) {
    let slides = document.getElementsByClassName("slide");

    if (n > slides.length) {
        slideIndex1 = 1;
    }

    if (n < 1) {
        slideIndex1 = slides.length;
    }

    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    slides[slideIndex1 - 1].style.display = "block";
}

// Xử lý bấm nút chuyển slide trước đó
function prevSlide() {
  // Tìm slide trước đó
  var nextSlideIndex;

  showSlides(slideIndex1 -= 1);
  // Nếu chỉ số slide là 0, về slide cuối
  if (currentSlideIndex === 0) {
    nextSlideIndex = slideArray.length - 1;
  } else {
    // Nếu không thì giảm chỉ số đi 1
    nextSlideIndex = currentSlideIndex - 1;
  }

  // Ẩn slide hiện tại, hiện slide "currentSlideIndex"
  document.getElementById("slide" + nextSlideIndex).style.left = "-100%";
  document.getElementById("slide" + currentSlideIndex).style.left = 0;

  // Thêm class để chuyển slide có animation đã định nghĩa ở bước 3
    document
    .getElementById("slide" + nextSlideIndex)
    .setAttribute("class", "singleSlide slideInLeft");
  document
    .getElementById("slide" + currentSlideIndex)
    .setAttribute("class", "singleSlide slideOutRight");

  // Cập nhật giá trị slide hiện tại
  currentSlideIndex = nextSlideIndex;
}

// Xử lý bấm nút chuyển slide tiếp theo
// Cách xử lý tương tự như prevSlide đã trình bày ở trên
function nextSlide() {
  var nextSlideIndex;
  showSlides(slideIndex1 += 1);
  if (currentSlideIndex === slideArray.length - 1) {
    nextSlideIndex = 0;
  } else {
    nextSlideIndex = currentSlideIndex + 1;
  }

  document.getElementById("slide" + nextSlideIndex).style.left = "100%";
  document.getElementById("slide" + currentSlideIndex).style.left = 0;

  document
    .getElementById("slide" + nextSlideIndex)
    .setAttribute("class", "singleSlide slideInRight");
  document
    .getElementById("slide" + currentSlideIndex)
    .setAttribute("class", "singleSlide slideOutLeft");

  currentSlideIndex = nextSlideIndex;
}


